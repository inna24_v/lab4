#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#ifndef LINKEDLIST_H
#define LINKEDLIST_H

typedef struct LinkedlistElement LinkedlistElement;
typedef struct LinkedList LinkedList;

struct LinkedlistElement{
    int value;
    LinkedlistElement *NextElement;
};


struct LinkedList{
    int length;
    LinkedlistElement* firstElement;
    LinkedlistElement* lastElement;
};

LinkedList* list_create_length(int length);

LinkedList* list_create(int value);

void list_add_front(int value, LinkedList *l);

void list_add_back(int value, LinkedList *l);

int list_get(int index, LinkedList* l);

void list_free(LinkedList* l);

int list_length(LinkedList* l);

LinkedlistElement* list_LinkedlistElement_at(int index, LinkedList* l);

long list_sum(LinkedList* l);

void print_list(LinkedList* l);

#endif

LinkedList* list_create(int value){
    LinkedList* ll = (LinkedList*)malloc(sizeof(LinkedList));
    ll->firstElement = (LinkedlistElement*) malloc(sizeof(LinkedlistElement));
    ll->length = 1;
    ll->firstElement->value = value;
    ll->firstElement->NextElement = NULL;
    return ll;
}

LinkedList* list_create_length(int length){
    LinkedList* ll = (LinkedList*)malloc(sizeof(LinkedList));
	int i, el;
    ll->length = length;
    ll->firstElement = (LinkedlistElement*)malloc(sizeof(LinkedlistElement));
	printf("Enter element: ");
	scanf("%d", &el);
	ll->firstElement->value=el;
    if (length >=2) {
        LinkedlistElement *lle = (LinkedlistElement*)malloc(sizeof(LinkedlistElement));
        ll->firstElement->NextElement = lle;
		printf("Enter element: ");
		scanf("%d", &el);
		ll->firstElement->NextElement->value=el;
        for (i = 2; i < length; i++) {
            lle->NextElement = (LinkedlistElement*)malloc(sizeof(LinkedlistElement));
            lle = lle->NextElement;
			printf("Enter element: ");
			scanf("%d", &el);
			lle->value=el;
        }
        ll->lastElement = lle;
        ll->lastElement->NextElement = NULL;
    }

    return ll;
}

void list_add_front(int value, LinkedList *l){
	LinkedlistElement *newNextElement;
    if (l->length == 0){
        l->firstElement = (LinkedlistElement*)malloc(sizeof(LinkedlistElement));
        l->firstElement->NextElement = NULL;
        l->firstElement->value = value;
        l->length++;
        return;
    }
    newNextElement = l->firstElement; 
    l->firstElement = (LinkedlistElement*)malloc(sizeof(LinkedlistElement));
    l->firstElement->value = value;
    l->firstElement->NextElement = newNextElement;
    if (l->length == 1){
        l->lastElement = newNextElement;
    }
    l->length++;
}

void list_add_back(int value, LinkedList *l){
	LinkedlistElement* newLast;
	LinkedlistElement* newLastElement;
    if (l->length==0){
        l->firstElement = malloc(sizeof(LinkedlistElement));
        l->firstElement->value = value;
        l->firstElement->NextElement = NULL;
        l->length++;
        return;
    }

    if (l->length ==1){
        newLast = (LinkedlistElement* ) malloc(sizeof(LinkedlistElement));
        newLast->value = value;
        l->lastElement = newLast;
        l->firstElement->NextElement = newLast;
        l->length++;
        return;
    }

    newLastElement =(LinkedlistElement*) malloc(sizeof(LinkedlistElement));
    newLastElement->NextElement = NULL;
    newLastElement->value = value;
    l->lastElement->NextElement = newLastElement;
    l->lastElement = newLastElement;
    l->length++;
}


int list_get(int index, LinkedList* l){
	int i;
	LinkedlistElement* element = l->firstElement;
    if (index >= l->length) return 0;
    if (l->length == 1) return l->firstElement->value;
    for (i = 0; i < index; i++)
        element = element->NextElement;
    return element->value;
}

void list_free(LinkedList* l){
    LinkedlistElement* element = l->firstElement;
    LinkedlistElement* nextElement;
	int i;
    for (i = 0; i < l->length; i++){
        nextElement = element->NextElement;
        free(element);
        element = nextElement;
    }
    free(l);
}

int list_length(LinkedList* l){
    return l->length;
}

LinkedlistElement* list_LinkedlistElement_at(int index, LinkedList* l){
	int i;
	LinkedlistElement* element = l->firstElement;
    if(index > l->length - 1) return NULL;
    for (i = 1; i < index; i++){
        element = element->NextElement;
    }
    return element;
}

long list_sum(LinkedList* l){
	LinkedlistElement* element = l->firstElement;
    int sum = element->value;
	int i;
    if (l->length == 0) return 0;
    for (i = 1; i < l->length; i++){
        element = element->NextElement;
        sum += element->value;
    }
    return sum;
}

void print_list(LinkedList* l){
	int i;
	printf("List:");
	for(i=0; i<list_length(l); i++)
		printf(" %d", list_get(i, l));
	printf("\n");
}

int main(int argc, char** argv) {
	int val;
	LinkedList* list_link= NULL;
	printf("Enter the value: ");
	scanf("%d", &val) ;
	list_link = list_create(val);
	while (scanf("%d", &val) == 1 ) {
        list_add_front(val, list_link);
    }

	printf("Sum of elements: %ld\n", list_sum(list_link));
	
	list_free(list_link);
	return 0;
}
