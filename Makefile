all: main
	
main: main.c
	gcc -o main -ansi -pedantic -Wall -Werror main.c
clean:
	rm -f main
